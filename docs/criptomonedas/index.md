---
title: Inicio
---

# Criptomonedas

## Bitcoin

### Wallets

Recuerden hacer backup de sus claves privadas.

📖 = Es open source

⚡ = Soporta Lightning Network

👁 = Soporta CoinJoins

- [Phoenix](https://phoenix.acinq.co/) (Android/iOS) 📖⚡
- [Muun](https://muun.com/) (Android/iOS) 📖⚡
- [Breez](https://breez.technology/) (Android/iOS) 📖⚡
- [BlueWallet](https://bluewallet.io/) (Android/iOS/Mac) 📖⚡
- [Samourai](https://samouraiwallet.com/) (Android) 📖👁
- [Sparrow](https://sparrowwallet.com/) (Linux/Mac/Windows) 📖👁
- [Wasabi](https://wasabiwallet.io/) (Linux/Mac/Windows) 📖👁
- [Electrum](https://electrum.org/) (Android/Linux/Mac/Windows) 📖⚡
- [Green](https://blockstream.com/green/) (Android/iOS/Linux/Mac/Windows) 📖
- [Aqua](https://aquawallet.io/) (Android/iOS) ⚡

#### Guardar montos grandes

- [Casa Keymaster](https://keys.casa/keymaster/) (Android/iOS)
- [Unchained Capital](https://unchained-capital.com/vaults/) (Web)
- [Caravan](https://unchained-capital.github.io/caravan/) (Web)

#### Hardware wallets

- [Trezor](https://trezor.io/)
- [Ledger](https://www.ledger.com/)
- [Coldcard](https://coldcardwallet.com/)

### Pagos anónimos

#### Monero

Podemos pagar con los Bitcoin de otro con Monero (criptomoneda enfocada en la privacidad) usando [FixedFloat](https://fixedfloat.com/), [UnstoppableSwap](https://unstoppableswap.net/) o [Changelly](https://changelly.com/). Primero tenes que bajarte una [wallet de Monero](https://www.getmonero.org/downloads/), comprar Monero de la forma que prefieras (ver lista de exchanges abajo), mandartelos y hacer el pago a la direccion de Bitcoin que quieras con los servicios mencionados arriba, mandando Monero como input.

También deberías usar [Tor Browser](https://www.torproject.org/) o [Tails OS](https://tails.boum.org/).

#### CoinJoins (avanzado)

Se pueden mezclar tus bitcoins con los de otros haciendo coinjoins, para esto tenemos wallets como [Samourai](https://samouraiwallet.com/), [Sparrow](https://sparrowwallet.com/) y [Wasabi](https://wasabiwallet.io/), lo considero mas avanzado porque hay que tener mas cuidado de no volver a mezclar con una direccion vinculada a tu identidad el cambio que te queda despues de hacer un pago con bitcoins limpios. Lo recomendable es no volver a vincular nunca a vos los bitcoins limpios, pero si es necesario, es buena idea volver a limpiarlos antes y entender que los exchanges con KYC mas vigilantes pueden cuestionarte si es evidente que les mandaste bitcoins que limpiaste, por este motivo yo prefiero mantener estos bitcoins fuera de servicios centralizados o con custodia, y si es necesario, uso tecnicas de distraccion para que no sea evidente que los bitcoins vienen de un coinjoin, como moverlos un poco entre direcciones distintas y meterlos desde un canal de Lightning u otra cadena como RSK, Liquid, Ethereum, etc.

## Ethereum

### Wallets

Recuerden hacer backup de sus claves privadas.

📖 = Es open source

- [Rabby](https://rabby.io/) (Chrome/Brave) 📖
- [Rainbow](https://rainbow.me/) (Android/iOS/Chrome/Brave/Firefox) 📖
- [Phantom](https://phantom.app/) (Android/iOS/Chrome/Brave/Firefox) 📖
- [Zerion](https://zerion.io/) (Android/iOS/Mac/Chrome/Brave) 📖
- [MetaMask](https://metamask.io/) (Android/iOS/Chrome/Brave/Firefox) 📖
- [Liquality](https://liquality.io/) (Chrome/Brave) 📖

#### Guardar montos grandes

- [Gnosis Safe](https://gnosis-safe.io/) (Web/Android/iOS/Linux/Mac/Windows)

#### Hardware wallets

- [Trezor](https://trezor.io/)
- [Ledger](https://www.ledger.com/)

### Dapps

- [Mean Finance](https://mean.finance/)
- [Curve](https://curve.fi/)
- [Uniswap](https://app.uniswap.org/)
- [Paraswap](https://app.paraswap.io/)
- [dYdX](https://trade.dydx.exchange/)
- [1inch](https://app.1inch.io/)
- [CoW Swap](https://swap.cow.fi/)
- [Aave](https://app.aave.com/)
- [Compound](https://app.compound.finance/)
- [Yearn](https://yearn.finance/)
- [DeFi Saver](https://app.defisaver.com/)
- [Instadapp](https://defi.instadapp.io/)

## RSK

Sidechain de Bitcoin con DeFi y otros smart contracts.

### Wallets

Recuerden hacer backup de sus claves privadas.

📖 = Es open source

- [Rabby](https://rabby.io/) (Chrome/Brave) 📖
- [Defiant](https://defiantapp.tech/) (Android/iOS)
- [Liquality](https://liquality.io/) (Chrome/Brave) 📖
- [MetaMask](https://metamask.io/) (Android/iOS/Chrome/Brave/Firefox) 📖 [Agregar RSK a MetaMask](https://metamask-landing.rifos.org/), [más info](https://developers.rsk.co/wallet/use/metamask/)

#### Hardware wallets

- [Trezor](https://trezor.io/)
- [Ledger](https://www.ledger.com/)

### Pasar BTC a RBTC

Para usar tus BTC en RSK hay que convertirlos a RBTC, hay distintos metodos disponibles:

- Usar FastBTC de [Sovryn](https://sovryn.app/)

Vas a Portfolio y apretas FastBTC, te genera una direccion BTC donde podes mandarlos y se convierten a RBTC en tu wallet de RSK.

- Hacer un swap en [Liquality](https://liquality.io/)

Esta wallet tiene integrado un exchange que soporta RBTC.

- Usar el metodo nativo PowPeg

Es el metodo mas barato y seguro por ser el nativo de RSK, pero por ahora tambien es el mas lento ya que toma alrededor de 1 dia, la forma mas facil de hacerlo es con la wallet [Defiant](https://defiantapp.tech/).

### Dapps

- [Money On Chain](https://moneyonchain.com/) Stablecoin descentralizada respaldada en Bitcoin con token para generar interes en Bitcoin
- [Sovryn](https://www.sovryn.app/) Exchange y prestamos descentralizados
- [Sovryn Bridge](https://bridge.sovryn.app/) Bridge para pasar a RSK tokens de Ethereum y Binance Smart Chain
- [RSK Bridge](https://tokenbridge.rsk.co/) Bridge para pasar a RSK tokens de Ethereum
- [RSK Swap](https://rskswap.com/) Exchange descentralizado
- [RIF Name Service](https://manager.rns.rifos.org/) Registro de dominios .rsk

## Servicios globales

### Trading exchanges

⚡ = Soporta Lightning Network

🛡️ = Soporta Monero

🥷🏻 = Se puede usar sin KYC

- [Kraken](https://www.kraken.com/) ⚡🛡️
- [Bitfinex](https://www.bitfinex.com/) ⚡🛡️
- [Binance](https://www.binance.com/) ⚡🛡️
- [FixedFloat](https://fixedfloat.com/) ⚡🛡️🥷🏻
- [Changelly](https://changelly.com/) 🛡️🥷🏻
- [SideShift.ai](https://sideshift.ai/) ⚡🛡️🥷🏻
- [Flyp.me](https://flyp.me/) ⚡🛡️🥷🏻
- [Coinbase Pro](https://pro.coinbase.com/)
- [Gemini](https://gemini.com/)
- [Bitstamp](https://www.bitstamp.net/)
- [OKCoin](https://www.okcoin.com/) ⚡
- [OKX](https://www.okx.com/) ⚡🛡️
- [UnstoppableSwap](https://unstoppableswap.net/) 🛡️🥷🏻
- [ShapeShift](https://shapeshift.com/) 🥷🏻
- [THORSwap](https://thorswap.finance/) 🥷🏻

### Gift Cards

- [Bitrefill](https://www.bitrefill.com/)

### Debit Cards

- [Moon](https://paywithmoon.com/)

### Procesadores de pago

- [OpenNode](https://www.opennode.com/)
- [Bitfinex Pay](https://pay.bitfinex.com/)
- [Coinbase Commerce](https://commerce.coinbase.com/)
- [NOWPayments](https://nowpayments.io/)
- [CoinPayments](https://www.coinpayments.net/)
- [BitPay](https://bitpay.com/)
- [CoinGate](https://coingate.com/)
- [BTCPay Server](https://btcpayserver.org/)

## Servicios por país

### Argentina

#### Comprar criptos (Transferencia bancaria)

- [Binance P2P](https://www.binance.com/)
- [CriptoYa](https://criptoya.com/)
- [CoinMonitor](https://coinmonitor.info/)
- [Cryptosaurio](https://www.cryptosaurio.com/)

### Europa

#### Comprar criptos (SEPA)

⚡ = Soporta Lightning Network

🛡️ = Soporta Monero

- [Kraken](https://www.kraken.com/) ⚡🛡️
- [Gemini](https://gemini.com/)
- [Crypto.com](https://crypto.com/)
- [OKCoin](https://www.okcoin.com/) ⚡
- [Coinbase](https://www.coinbase.com/)
- [Circle](https://www.circle.com/)
- [Nexo](https://nexo.com/)
- [Bity](https://bity.com/)
- [Bitstamp](https://www.bitstamp.net/)
- [Mt Pelerin](https://www.mtpelerin.com/)

### Estados Unidos

#### Comprar criptos (Wire/ACH)

⚡ = Soporta Lightning Network

🛡️ = Soporta Monero

- [Kraken](https://www.kraken.com/) ⚡🛡️
- [Cash App](https://cash.app/) ⚡
- [Strike](https://strike.me/) ⚡
- [River Financial](https://river.com/) ⚡
- [Gemini](https://gemini.com/)
- [OKCoin](https://www.okcoin.com/) ⚡
- [Coinbase](https://www.coinbase.com/)
- [Binance.US](https://www.binance.us/)
- [Crypto.com](https://crypto.com/)
- [Circle](https://www.circle.com/)
- [Bitstamp](https://www.bitstamp.net/)
